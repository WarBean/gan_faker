import mxnet as mx
from MLUtil.config import Config
from MLUtil.mxnet_misc import print_shape
from MLUtil.mxnet_symbol_block import FcBlock, ConvBlock, DconvBlock

def generate(config):
    config = config(
        use_pool = False,
    )
    config.layid = config.last_layid
    config.layid += 1; config.layer = FcBlock(config(num_hidden = 2 * 2 * 2048))
    config.layid += 1; config.layer = mx.sym.Reshape(config.layer, shape = (0, 2048, 2, 2), name = 'layer%d-reshape' % config.layid)
    if config.verbose_shape: print_shape(config.layer, config.input_shapes)
    num_filters = [1024, 512, 256, 128]
    config.num_filter = num_filters[0]
    #config.layid += 1; config.layer =  ConvBlock(config(kernel = 3, stride = 1, pad = 1))
    for rep in range(config.repeat_count):
        config.num_filter = num_filters[rep]
        config.layid += 1; config.layer = DconvBlock(config(kernel = 4, stride = 2, pad = 1))
        #config.layid += 1; config.layer = config.layer +  ConvBlock(config(kernel = 3, stride = 1, pad = 1))
        #config.layid += 1; config.layer = config.layer +  ConvBlock(config(kernel = 3, stride = 1, pad = 1))
        #config.layid += 1; config.layer = config.layer +  ConvBlock(config(kernel = 3, stride = 1, pad = 1))
        #if rep <= 0 or rep >= config.repeat_count - 2: config.layid += 1; config.layer = config.layer +  ConvBlock(config(kernel = 3, stride = 1, pad = 1))
    config.num_filter = config.image_channel_count
    config.layid += 1; content = ConvBlock(config(kernel = 3, stride = 1, pad = 1, use_bn = False, act_type = 'tanh'))
    return content

def discriminate(config):
    config = config.copy_fillup(
        num_filter = 128,
        use_pool = False,
        act_type = 'leaky',
        slope = 0.2,
    )
    num_filters = [128, 256, 512, 1024, 2048]
    config.layid = config.last_layid
    config.layid += 1; config.layer = ConvBlock(config(kernel = 3, stride = 1, pad = 1, use_bn = False))
    for rep in range(config.repeat_count):
        config.num_filter = num_filters[rep]
        config.layid += 1; config.layer = ConvBlock(config(kernel = 3, stride = 2, pad = 1))
        #config.layid += 1; config.layer = config.layer + ConvBlock(config(kernel = 3, stride = 1, pad = 1))
        #config.layid += 1; config.layer = config.layer + ConvBlock(config(kernel = 3, stride = 1, pad = 1))
        #config.layid += 1; config.layer = config.layer + ConvBlock(config(kernel = 3, stride = 1, pad = 1))
    #config.layid += 1; config.layer = mx.sym.Pooling(config.layer, global_pool = True, pool_type = 'avg', \
    #                                                 kernel = (2, 2), name = 'layer%d-global-pooling' % config.layid)
    #if config.verbose_shape: print_shape(config.layer, config.input_shapes)
    config.layid += 1; config.layer = mx.sym.Flatten(config.layer, name = 'layer%d-flatten' % config.layid)
    if config.verbose_shape: print_shape(config.layer, config.input_shapes)
    # Mini Batch Discrimination
    #from MLUtil.mxnet_symbol_block import MiniBatchDiscrimination
    #config.layid += 1; minibatch_dis = MiniBatchDiscrimination(config(tensor_size_B = 128, tensor_size_C = 128))
    #config.layid += 1; config.layer = mx.sym.Concat(config.layer, minibatch_dis, num_args = 2, dim = 1, name = 'layer%d-concat' % config.layid)
    #if config.verbose_shape: print_shape(config.layer, config.input_shapes)
    config.layid += 1; logit = FcBlock(config(num_hidden = 1, use_act = False))
    return logit

def get_symbol_gan(config):
    generator_input_shapes = { 'latent': config.input_shapes['latent'] }
    latent = mx.sym.Variable('latent')
    if config.verbose_shape: print_shape(latent, generator_input_shapes, name = 'latent')
    fake_content = generate(config(layer = latent, last_layid = 0, repeat_count = 4, input_shapes = generator_input_shapes))
    real_content = mx.sym.Variable('real_content')
    content = mx.sym.Concat(fake_content, real_content, dim = 0, name = 'content')
    if config.verbose_shape: print_shape(content, config.input_shapes, name = 'content')
    logit = discriminate(config(layer = content, last_layid = 100, repeat_count = 5))
    if config.verbose_shape: print_shape(logit, config.input_shapes, name = 'logit')
    return logit

def get_symbol_generator(config):
    latent = mx.sym.Variable('latent')
    if config.verbose_shape: print_shape(latent, config.input_shapes, name = 'latent')
    content = generate(config(layer = latent, last_layid = 0, repeat_count = 4))
    if config.verbose_shape: print_shape(content, config.input_shapes, name = 'content')
    return content

def get_symbol_discriminator(config):
    content = mx.sym.Variable('content')
    if config.verbose_shape: print_shape(content, config.input_shapes, name = 'content')
    logit = discriminate(config(layer = content, last_layid = 100, repeat_count = 5))
    if config.verbose_shape: print_shape(logit, config.input_shapes, name = 'logit')
    return logit

def get_symbol_logistic(config):
    logit = mx.sym.Variable('logit')
    label = mx.sym.Variable('label')
    logistic = mx.sym.LogisticRegressionOutput(data = logit, label = label, name = 'logistic')
    return logistic

if __name__ == '__main__':
    from MLUtil import mxnet_model_io
    config = Config('hyperparameter_gan_cifar10.json').inplace_overwrite(
        verbose_shape = True,
    )
    input_shapes_gan = { 'latent' : (config.batch_size, 100), 'real_content': (config.batch_size, config.image_channel_count, config.image_height, config.image_width) }
    input_shapes_gen = { 'latent' : (config.batch_size, 100) }
    input_shapes_dis = { 'content' : (config.batch_size, config.image_channel_count, config.image_height, config.image_width) }
    print('=' * 40 + '\nsymbol_gan:'); symbol_gan = get_symbol_gan(config(input_shapes = input_shapes_gan))
    print('=' * 40 + '\nsymbol_gen:'); symbol_gen = get_symbol_generator(config(input_shapes = input_shapes_gen))
    print('=' * 40 + '\nsymbol_dis:'); symbol_dis = get_symbol_discriminator(config(input_shapes = input_shapes_dis))
    print('=' * 40)
    executor_gan = mxnet_model_io.get_shared_executor(config(
        symbol = symbol_gan,
        ctx = mx.gpu(config.gpuid),
        input_shapes = input_shapes_gan,
    ))
