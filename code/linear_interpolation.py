import sys
import importlib
import numpy as np
import mxnet as mx
from PIL import Image
from MLUtil import misc
from MLUtil import mxnet_model_io
from local_misc import load_config

specific_json = sys.argv[1]
beg_load_id = int(sys.argv[2])
end_load_id = int(sys.argv[3])
seed = int(sys.argv[4]) if len(sys.argv) >= 5 else 1024
config = load_config('hyperparameter_gan_common.json', specific_json)
symbol = importlib.import_module('symbol_%s' % config.dataset)
if config.gpuid == -1: ctx = mx.cpu(0)
else: ctx = mx.gpu(config.gpuid)
input_shapes = { 'latent' : (1, 100) }
symbol_gen = symbol.get_symbol_generator(config(input_shapes = input_shapes))

mx.random.seed(seed)
outer_z_list = []
z1 = mx.random.normal(loc = 0, scale = config.latent_stdvar, ctx = ctx, shape = input_shapes['latent'])
for i in range(10):
    z2 = mx.random.normal(loc = 0, scale = config.latent_stdvar, ctx = ctx, shape = input_shapes['latent'])
    inner_z_list = [(1 - p) * z1 + p * z2 for p in np.arange(0, 1, 0.1)]
    outer_z_list.append(inner_z_list)
    z1 = z2
for load_id in range(beg_load_id, end_load_id + 1):
    print('load_id %d' % load_id)
    checkpoint_path = '../model/%s-%04d.params' % (config.checkpoint_prefix, load_id)
    param_dict = mxnet_model_io.load_checkpoint(checkpoint_path, ctx)
    executor_gen = mxnet_model_io.get_shared_executor(config(
        symbol = symbol_gen,
        ctx = ctx,
        input_shapes = input_shapes,
        shared_stuff = param_dict,
    ))
    outer_data_list = []
    for inner_z_list in outer_z_list:
        inner_data_list = []
        for z in inner_z_list:
            executor_gen.arg_dict['latent'][:] = z
            executor_gen.forward(is_train = False)
            output = executor_gen.outputs[0].asnumpy()[0]
            output = misc.safely_to_uint8(output * 128 + 128)
            if output.ndim == 2: output = output[0]
            else: output = output.swapaxes(0, 1).swapaxes(1, 2)
            inner_data_list.append(output)
        data = np.hstack(inner_data_list)
        outer_data_list.append(data)
    data = np.vstack(outer_data_list)
    image = Image.fromarray(data)
    image.save('../image/%s-interpolation-%04d.jpg' % (config.checkpoint_prefix, load_id))
