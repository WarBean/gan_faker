import os
import sys
import time
import importlib
import numpy as np
import mxnet as mx
from PIL import Image
from queue import deque
from MLUtil import misc
from MLUtil import mxnet_misc
from MLUtil import mxnet_model_io
from MLUtil.log import Log
from MLUtil.config import Config

def D_forward(is_train):
    batch = loader.next_batch()
    mx.random.normal(loc = 0, scale = config.latent_stdvar, out = executor_gan.arg_dict['latent'])
    executor_gan.arg_dict['real_content'][:] = batch.data
    executor_gan.forward(is_train)
    D_logit = executor_gan.outputs[0].asnumpy()
    D_fake_logit = D_logit[: config.batch_size]
    D_real_logit = D_logit[config.batch_size :]
    # fake logistic
    executor_fake_logistic.arg_dict['logit'][:] = D_fake_logit
    executor_fake_logistic.arg_dict['label'][:] = 0
    executor_fake_logistic.forward(is_train)
    D_fake_prob_output = executor_fake_logistic.outputs[0].asnumpy()
    # real logistic
    executor_real_logistic.arg_dict['logit'][:] = D_real_logit
    executor_real_logistic.arg_dict['label'][:] = 1
    executor_real_logistic.forward(is_train)
    D_real_prob_output = executor_real_logistic.outputs[0].asnumpy()
    # evaluate
    if not is_train:
        D_fake_loss = -np.mean(np.log(1 - D_fake_prob_output))
        D_real_loss = -np.mean(np.log(D_real_prob_output))
        D_fake_accu = np.mean(D_fake_prob_output < 0.5)
        D_real_accu = np.mean(D_real_prob_output > 0.5)
        D_fake_loss_window.append(D_fake_loss) if D_fake_loss != float('inf') else None
        D_real_loss_window.append(D_real_loss) if D_real_loss != float('inf') else None
        D_fake_accu_window.append(D_fake_accu)
        D_real_accu_window.append(D_real_accu)
        return D_fake_accu

def G_forward(is_train):
    batch = loader.next_batch()
    mx.random.normal(loc = 0, scale = config.latent_stdvar, out = executor_gan.arg_dict['latent'])
    executor_gan.arg_dict['real_content'][:] = batch.data
    executor_gan.forward(is_train)
    G_logit = executor_gan.outputs[0].asnumpy()
    G_fake_logit = G_logit[: config.batch_size]
    G_real_logit = G_logit[config.batch_size :]
    # fake logistic
    executor_fake_logistic.arg_dict['logit'][:] = G_fake_logit
    executor_fake_logistic.arg_dict['label'][:] = 1
    executor_fake_logistic.forward(is_train)
    G_fake_prob_output = executor_fake_logistic.outputs[0].asnumpy()
    # real logistic
    executor_real_logistic.arg_dict['logit'][:] = G_real_logit
    executor_real_logistic.arg_dict['label'][:] = 0
    executor_real_logistic.forward(is_train)
    G_real_prob_output = executor_real_logistic.outputs[0].asnumpy()
    # evaluate
    if not is_train:
        G_fake_loss = -np.mean(np.log(G_fake_prob_output))
        G_real_loss = -np.mean(np.log(1 - G_real_prob_output))
        G_fake_accu = np.mean(G_fake_prob_output > 0.5)
        G_real_accu = np.mean(G_real_prob_output < 0.5)
        G_fake_loss_window.append(G_fake_loss) if G_fake_loss != float('inf') else None
        G_real_loss_window.append(G_real_loss) if G_real_loss != float('inf') else None
        G_fake_accu_window.append(G_fake_accu)
        G_real_accu_window.append(G_real_accu)
        return G_fake_accu

def D_forward_backward():
    D_forward(is_train = True)
    # backward on gan
    executor_fake_logistic.backward()
    executor_real_logistic.backward()
    D_fake_logit_grad = executor_fake_logistic.grad_dict['logit']
    D_real_logit_grad = executor_real_logistic.grad_dict['logit']
    out_grad = mx.nd.concatenate([D_fake_logit_grad, D_real_logit_grad])
    executor_gan.backward(out_grads = out_grad)
    for i, name in enumerate(symbol_dis.list_arguments()):
        if name not in ['content', 'label']:
            grad = executor_gan.grad_dict[name]
            arg = executor_gan.arg_dict[name]
            D_updater(i, grad, arg)

def G_forward_backward():
    G_forward(is_train = True)
    # backward on gan
    executor_fake_logistic.backward()
    executor_real_logistic.backward()
    G_fake_logit_grad = executor_fake_logistic.grad_dict['logit']
    G_real_logit_grad = executor_real_logistic.grad_dict['logit']
    #G_real_logit_grad = mx.nd.zeros(ctx = ctx, shape = G_fake_logit_grad.shape)
    out_grad = mx.nd.concatenate([G_fake_logit_grad, G_real_logit_grad])
    executor_gan.backward(out_grads = out_grad)
    for i, name in enumerate(symbol_gen.list_arguments()):
        if name not in ['latent']:
            grad = executor_gan.grad_dict[name]
            arg = executor_gan.arg_dict[name]
            G_updater(i, grad, arg)

dataset = sys.argv[1]
symbol = importlib.import_module('symbol_%s' % dataset)
config = Config('hyperparameter_gan_%s.json' % dataset)
log = Log('../log/log_train_gan_%s.txt' % dataset)
log(open('hyperparameter_gan_%s.json' % dataset).read())
if dataset in ['cifar10']:
    from MLUtil import cifar10_data_loader
    loader = cifar10_data_loader.get_train_loader(config)
elif dataset in ['celebA']:
    import celebA_data_loader
    loader = celebA_data_loader.get_loader(config)
elif dataset in ['line']:
    import line_data_loader
    loader = line_data_loader.get_fake_line_loader(config)
elif dataset in ['mnist']:
    from MLUtil import mnist_data_loader
    loader = mnist_data_loader.get_train_loader(config(resize = (config.image_height, config.image_width)))
else:
    assert False, 'unkown dataset: %s' % dataset

if config.gpuid == -1: ctx = mx.cpu(0)
else: ctx = mx.gpu(config.gpuid)
uniform_init = mx.init.Uniform(scale = config.weight_scale)
normal_init = mx.init.Normal(sigma = config.weight_scale)
initializer_list = [
    [mxnet_misc.name_hasnot_keyword(['latent', 'content', 'label']), normal_init],
]
input_shapes_gen = { 'latent' : (config.plot_batch_size, 100) }
input_shapes_dis = { 'content' : (config.batch_size, config.image_channel_count, config.image_height, config.image_width) }
input_shapes_gan = { 'latent' : (config.batch_size, 100), 'real_content': input_shapes_dis['content'] }
input_shapes_logistic = { 'logit': (config.batch_size, 1) }
# use symbol_dis only for list_arguments below, won't create executor
symbol_gen = symbol.get_symbol_generator    (config(input_shapes = input_shapes_gen))
symbol_dis = symbol.get_symbol_discriminator(config(input_shapes = input_shapes_dis))
symbol_gan = symbol.get_symbol_gan          (config(input_shapes = input_shapes_gan))
symbol_logistic = symbol.get_symbol_logistic(config(input_shpaes = input_shapes_logistic))
executor_gan = mxnet_model_io.get_shared_executor(config(
    symbol = symbol_gan,
    ctx = ctx,
    input_shapes = input_shapes_gan,
    initializer_list = initializer_list,
))
executor_gen = mxnet_model_io.get_shared_executor(config(
    symbol = symbol_gen,
    ctx = ctx,
    input_shapes = input_shapes_gen,
    shared_stuff = executor_gan,
    share_name_pred = mxnet_misc.name_hasnot_keyword(['latent'])
))
executor_fake_logistic = mxnet_model_io.get_shared_executor(config(
    symbol = symbol_logistic,
    ctx = ctx,
    input_shapes = input_shapes_logistic,
))
executor_real_logistic = mxnet_model_io.get_shared_executor(config(
    symbol = symbol_logistic,
    ctx = ctx,
    input_shapes = input_shapes_logistic,
))

lr_sum = config.learning_rate
D_opt = mx.optimizer.Adam(
    learning_rate = lr_sum * config.lr_init_factor,
    beta1 = config.momentum,
    wd = config.weight_decay,
    rescale_grad = 1.0 / config.batch_size)
D_updater = mx.optimizer.get_updater(D_opt)
G_opt = mx.optimizer.Adam(
    learning_rate = lr_sum - D_opt.lr,
    beta1 = config.momentum,
    wd = config.weight_decay,
    rescale_grad = 1.0 / config.batch_size)
G_updater = mx.optimizer.get_updater(G_opt)
time_window = deque(maxlen = 5)
D_real_loss_window = deque(maxlen = config.window_length * config.D_iter_count)
D_fake_loss_window = deque(maxlen = config.window_length * config.D_iter_count)
G_fake_loss_window = deque(maxlen = config.window_length * config.G_iter_count)
G_real_loss_window = deque(maxlen = config.window_length * config.G_iter_count)
D_real_accu_window = deque(maxlen = config.window_length * config.D_iter_count)
D_fake_accu_window = deque(maxlen = config.window_length * config.D_iter_count)
G_fake_accu_window = deque(maxlen = config.window_length * config.G_iter_count)
G_real_accu_window = deque(maxlen = config.window_length * config.G_iter_count)
who_window = deque(maxlen = 100)
sample_latent = mx.random.normal(loc = 0, scale = config.latent_stdvar, ctx = ctx, shape = executor_gen.arg_dict['latent'].shape)

beg_time = time.time()
last_time = beg_time
save_id = 0
checkpoint_path = '../model/gan-9999.params'
log('save checkpoint to %s' % checkpoint_path)
mxnet_model_io.save_checkpoint(checkpoint_path, executor_gan)
log('remove checkpoint %s' % checkpoint_path)
os.remove(checkpoint_path)
who = 'D'
for it in range(config.iter_count):
    it += 1
    D_forward(False)
    G_forward(False)
    D_metric = misc.mean(D_fake_accu_window, default = 0.5)
    G_metric = misc.mean(G_fake_accu_window, default = 0.5)
    #if G_metric > 0.51:
    #    D_forward_backward()
    #    who = 'D'
    #elif G_metric < 0.49:
    #    G_forward_backward()
    #    who = 'G'
    #elif who == 'D':
    #    D_forward_backward()
    #else:
    #    G_forward_backward()
    if it % 9 == 0:
        D_forward_backward()
        who = 'D'
    else:
        G_forward_backward()
        who = 'G'
    who_window.append(int(who == 'D'))
    if it % config.report_interval == 0:
        curr_time = time.time()
        total_past_time = curr_time - beg_time
        time_window.append((curr_time - last_time) / config.batch_size / config.report_interval * 1e3)
        last_time = curr_time
        sample_time = misc.mean(time_window)
        D_fake_loss = misc.mean(D_fake_loss_window, default = -1)
        D_real_loss = misc.mean(D_real_loss_window, default = -1)
        G_fake_loss = misc.mean(G_fake_loss_window, default = -1)
        G_real_loss = misc.mean(G_real_loss_window, default = -1)
        D_fake_accu = misc.mean(D_fake_accu_window, default = -1)
        D_real_accu = misc.mean(D_real_accu_window, default = -1)
        G_fake_accu = misc.mean(G_fake_accu_window, default = -1)
        G_real_accu = misc.mean(G_real_accu_window, default = -1)
        who_D_ratio = misc.mean(who_window)
        who_G_ratio = 1 - who_D_ratio

        log('iter %d, past %ds, each %dms, lr %.08f, D_loss %.04f/%.04f, G_loss %.04f/%.04f, D_accu %.02f/%.02f, G_accu %.02f/%.02f, who %s, who ratio %.02f/%.02f' %
            (it, total_past_time, sample_time, D_opt.lr, D_fake_loss, D_real_loss, G_fake_loss, G_real_loss, D_fake_accu, D_real_accu, G_fake_accu, G_real_accu, who, who_D_ratio, who_G_ratio))

    if it % config.save_interval == 0:
        save_id += 1
        checkpoint_path = '../model/gan-%04d.params' % save_id
        mxnet_model_io.save_checkpoint(checkpoint_path, executor_gan, mxnet_misc.name_hasnot_keyword(['latent', 'content', 'label']))
        log('============================================================')
        log('save checkpoint to %s' % checkpoint_path)
        log('============================================================')

    if it % config.plot_interval == 0:
        executor_gen.arg_dict['latent'][:] = sample_latent
        executor_gen.forward(is_train = False)
        output = executor_gen.outputs[0].asnumpy()
        if dataset in ['line', '']:
            for bat_id in range(config.plot_batch_size):
                norm_data = output[bat_id, 0]
                data = norm_data * 128 + 128
                image = Image.fromarray(misc.safely_to_uint8(data))
                image.save('../image/%s-fake-iter%06d-sample%04d.jpg' % (dataset, it, bat_id))
        elif dataset in ['cifar10', 'celebA']:
            data_list = []
            for bat_id in range(config.plot_batch_size):
                norm_data = output[bat_id].swapaxes(0, 1).swapaxes(1, 2)
                data = norm_data * 128 + 128
                data_list.append(data)
            data = np.hstack(data_list)
            image = Image.fromarray(misc.safely_to_uint8(data))
            image.save('../image/temp/%s-fake-iter%06d.jpg' % (dataset, it))
        elif dataset in ['mnist']:
            data_list = []
            for bat_id in range(config.plot_batch_size):
                norm_data = output[bat_id, 0]
                data = norm_data
                data_list.append(data)
            data = np.hstack(data_list) * 128 + 128
            image = Image.fromarray(misc.safely_to_uint8(data))
            image.save('../image/%s-fake-iter%06d.jpg' % (dataset, it))
        else:
            assert False

    if it in config.lr_decay_points:
        lr_sum /= config.lr_decay_factor
        G_opt.lr /= config.lr_decay_factor
        D_opt.lr /= config.lr_decay_factor

    if it > config.adjust_lr_begin and it % config.adjust_lr_interval == 0:
        if D_fake_loss < G_fake_loss:
            delta = min(D_opt.lr, lr_sum * config.adjust_lr_factor)
            D_opt.lr -= delta
            G_opt.lr += delta
        else:
            delta = min(G_opt.lr, lr_sum * config.adjust_lr_factor)
            G_opt.lr -= delta
            D_opt.lr += delta

log.close()
