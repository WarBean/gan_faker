import os
import glob
import random
import numpy as np
from PIL import Image
from MLUtil import misc
from MLUtil.config import Config
from MLUtil.data_loader import DataLoaderOnTheGround as DataLoader

def crop(image, crop_width, crop_height):
    width, height = image.size
    w_margin = (width - crop_width) // 2
    h_margin = (height - crop_height) // 2
    box = (w_margin, h_margin, w_margin + crop_width, h_margin + crop_height)
    return image.crop(box)

def worker(loader):
    data_list = []
    while not loader.exit.is_set():
        path = loader.task_queue.get()
        image = Image.open(path)
        image = crop(image, loader.config.crop_width, loader.config.crop_height)
        image = image.resize((loader.config.image_width, loader.config.image_height))
        data = (np.array(image).swapaxes(2, 1).swapaxes(1, 0).astype('float32') - 128.0) / 128.0
        data_list.append(data)
        if len(data_list) == loader.config.batch_size:
            data_batch = np.stack(data_list)
            batch = { 'data': data_batch }
            loader.batch_queue.put(batch)
            data_list.clear()

def get_loader(config):
    config = config.copy_fillup(aug_config = [], init_shuffle = False)
    task_list = glob.glob(os.path.join(config.celebA_path, '*.jpg'))
    if config.init_shuffle:
        random.shuffle(task_list)
    loader = DataLoader(config(
        worker = worker,
        task_list = task_list,
        aug_funcs = [],
    ))
    return loader

get_gan_loader = get_loader

if __name__ == '__main__':
    config = Config('hyperparameter_gan_celebA.json')
    config.batch_size = 128
    loader = get_loader(config)
    batch = loader.next_batch()
    for i in range(config.batch_size):
        image = misc.normarray_to_rgbimage(batch.data[i])
        image.save('../image/celebA_image_%03d.jpg' % i)
