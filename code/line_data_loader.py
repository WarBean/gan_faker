import glob
import random
import numpy as np
from PIL import Image
from MLUtil import misc
from MLUtil.config import Config
from MLUtil.data_loader import DataLoaderOnTheGround

def worker(loader):
    data_list = []
    while not loader.exit.is_set():
        path = loader.task_queue.get()
        image = Image.open(path)
        if image.mode != loader.config.image_mode:
            image = image.convert(mode = loader.config.image_mode)
        raw_data = np.array(image).astype('int32')
        norm_data = (raw_data - 128.0) / 128.0
        if loader.config.image_mode != 'RGB':
            norm_data = np.expand_dims(norm_data, 2)
        norm_data = norm_data.swapaxes(2, 1).swapaxes(1, 0)
        width = norm_data.shape[-1]
        if width > loader.config.image_width:
            offset = random.randrange(width - loader.config.image_width + 1)
            norm_data = norm_data[:, :, offset : loader.config.image_width + offset]
        data_list.append(norm_data)
        if len(data_list) == loader.config.batch_size:
            data_batch = np.stack(data_list)
            batch = { 'data': data_batch }
            loader.batch_queue.put(batch)
            data_list.clear()

def get_fake_line_loader(config):
    return DataLoaderOnTheGround(config(
        worker = worker,
        task_list = glob.glob('../data/ocr_fake_line/*.jpg')
    ))

get_gan_loader = get_fake_line_loader

if __name__ == '__main__':
    config = Config('hyperparameter_gan.json')
    fake_line_loader = get_fake_line_loader(config)
    batch = fake_line_loader.next_batch()
    for bat_id in range(config.batch_size):
        raw_data = batch.data[bat_id] * 128 + 128
        show_data = np.repeat(raw_data, 3, axis = 0)
        show = misc.safely_to_uint8(show_data).swapaxes(0, 1).swapaxes(1, 2)
        Image.fromarray(show).save('../image/ocr_fake_line_sample_%d.png' % bat_id)
