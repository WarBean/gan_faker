import os
import sys
import time
import importlib
from gan import GAN
from MLUtil.log import Log
from MLUtil.timer import Timer
from local_misc import load_config

specific_json = sys.argv[1]
config = load_config('hyperparameter_gan_common.json', specific_json)
log = Log('../log/log_train_%s.txt' % config.checkpoint_prefix)
log(open(specific_json).read())
data_loader_module = importlib.import_module('%s_data_loader' % config.dataset)
loader = data_loader_module.get_gan_loader(config(batch_size = config.batch_size_real))
gan = GAN(config, loader)

save_id = 0
checkpoint_path = gan.save_checkpoint(save_id)
log('save checkpoint to %s' % checkpoint_path)
os.remove(checkpoint_path)
log('remove checkpoint %s' % checkpoint_path)
timer = Timer(scale = 1000 / config.batch_size_dis)

gan.visualize_fake(0)
log('visualize fake')
for it in range(config.iter_count):
    it += 1
    who = gan.train()
    gan.update_vbn()
    gan.evaluate()
    timer.record()
    curr_time = time.time()

    if it % config.report_interval == 0:
        log('iter %d, past %ds, each %dms, lr %.08f/%.08f, accu %.02f/%.02f, mbf error %.04f, who %s, who ratio %.02f/%.02f' %
            (it, timer.past(), timer.interval(), gan.opt_dis.lr, gan.opt_gen.lr, gan.real_accu(), gan.fake_accu(), gan.mbf_error(), who, gan.D_ratio(), gan.G_ratio()))

    if it % config.save_interval == 0:
        save_id += 1
        checkpoint_path = gan.save_checkpoint(save_id)
        log('save checkpoint to %s' % checkpoint_path)

    if it % config.visualize_interval == 0:
        gan.visualize_fake(it)
        log('visualize fake')

    if it in config.lr_decay_points:
        gan.decay_leanring_rate()
