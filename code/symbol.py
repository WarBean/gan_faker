import mxnet as mx
from MLUtil.mxnet_misc import print_shape
from MLUtil.mxnet_symbol_block import FcBlock, ConvBlock, DconvBlock

def generate(config):
    config = config(use_pool = False)
    config.layid = config.last_layid
    s = config.image_height // 2 ** 4
    config.layid += 1; config.layer = FcBlock(config(num_hidden = s * s * 1024))
    config.layid += 1; config.layer = mx.sym.Reshape(config.layer, shape = (0, 1024, s, s), name = 'layer%d-reshape' % config.layid)
    if config.verbose_shape: print_shape(config.layer, config.input_shapes)
    config.layid += 1; config.layer = DconvBlock(config(num_filter = 512, kernel = 4, stride = 2, pad = 1))
    config.layid += 1; config.layer = DconvBlock(config(num_filter = 256, kernel = 4, stride = 2, pad = 1))
    config.layid += 1; config.layer = DconvBlock(config(num_filter = 128, kernel = 4, stride = 2, pad = 1))
    config.layid += 1; content = DconvBlock(config(num_filter = config.channels, kernel = 4, stride = 2, pad = 1, use_bn = False, act_type = 'tanh'))
    return content

def discriminate(config):
    config = config(use_pool = False, act_type = 'leaky', slope = 0.2)
    config.layid = config.last_layid
    config.layid += 1; config.layer = ConvBlock(config(num_filter =  128, kernel = 5, stride = 2, pad = 2, use_bn = False))
    config.layid += 1; config.layer = ConvBlock(config(num_filter =  256, kernel = 5, stride = 2, pad = 2))
    config.layid += 1; config.layer = ConvBlock(config(num_filter =  512, kernel = 5, stride = 2, pad = 2))
    config.layid += 1; config.layer = ConvBlock(config(num_filter = 1024, kernel = 5, stride = 2, pad = 2))
    config.layid += 1; config.layer = mx.sym.Flatten(config.layer, name = 'layer%d-flatten' % config.layid)
    if config.verbose_shape: print_shape(config.layer, config.input_shapes)
    config.layid += 1
    real_slice = mx.sym.slice_axis(config.layer, axis = 0, begin = 0, end = config.batch_size_real, name = 'layer%d-real-slice' % config.layid)
    fake_slice = mx.sym.slice_axis(config.layer, axis = 0, begin = config.batch_size_real, end = config.batch_size_dis, name = 'layer%d-fake-slice' % config.layid)
    if config.verbose_shape: print_shape(real_slice, config.input_shapes, name = 'real_slice')
    if config.verbose_shape: print_shape(fake_slice, config.input_shapes, name = 'fake_slice')
    # Feature Matching
    real_mean_feature = mx.sym.sum(real_slice, axis = 0, keepdims = False, name = 'layer%d-real-sum-feature' % config.layid) / (config.batch_size_real)
    fake_mean_feature = mx.sym.sum(fake_slice, axis = 0, keepdims = False, name = 'layer%d-fake-sum-feature' % config.layid) / (config.batch_size_fake)
    if config.verbose_shape: print_shape(real_mean_feature, config.input_shapes, name = 'real_mean_feature')
    if config.verbose_shape: print_shape(fake_mean_feature, config.input_shapes, name = 'fake_mean_feature')
    if config.improve_technique.minibatch_discrimination:
        # Mini Batch Discrimination
        from MLUtil.mxnet_symbol_block import MiniBatchDiscrimination
        config.layid += 1;
        real_mutual_feature = MiniBatchDiscrimination(config(layer = real_slice, infix = 'real', tensor_size_B = 100, tensor_size_C = 5))
        fake_mutual_feature = MiniBatchDiscrimination(config(layer = fake_slice, infix = 'fake', tensor_size_B = 100, tensor_size_C = 5))
        if config.verbose_shape: print_shape(real_mutual_feature, config.input_shapes, name = 'real_mutual_feature')
        if config.verbose_shape: print_shape(fake_mutual_feature, config.input_shapes, name = 'fake_mutual_feature')
        config.layid += 1; mutual_feature = mx.sym.Concat(real_mutual_feature, fake_mutual_feature, num_args = 2, dim = 0, name = 'layer%d-concat-real-fake' % config.layid)
        if config.verbose_shape: print_shape(mutual_feature, config.input_shapes)
        config.layid += 1; config.layer = mx.sym.Concat(config.layer, mutual_feature, num_args = 2, dim = 1, name = 'layer%d-concat-raw-mutual' % config.layid)
        if config.verbose_shape: print_shape(config.layer, config.input_shapes)
    config.layid += 1; logit = FcBlock(config(num_hidden = 1, use_act = False))
    output = mx.sym.Group([logit, real_mean_feature, fake_mean_feature])
    return output

def get_symbol_vbn(config):
    latent = mx.sym.Variable('latent')
    content = generate(config(layer = latent, last_layid = 0))
    return mx.sym.BlockGrad(content, name = 'block-grad')

def get_symbol_gen(config):
    latent = mx.sym.Variable('latent')
    return generate(config(layer = latent, last_layid = 0))

def get_symbol_dis(config):
    content = mx.sym.Variable('content')
    return discriminate(config(layer = content, last_layid = 100))

def get_symbol_clf(config):
    data = mx.sym.Variable('data')
    label = mx.sym.Variable('label')
    logistic = mx.sym.LogisticRegressionOutput(data = data, label = label, name = 'clf')
    return logistic

def get_symbol_mbf(config):
    real_mbf = mx.sym.Variable('real_mbf')
    fake_mbf = mx.sym.Variable('fake_mbf')
    linear = mx.sym.LinearRegressionOutput(data = real_mbf, label = fake_mbf, grad_scale = config.mbf_grad_scale, name = 'linear')
    return linear

if __name__ == '__main__':
    from MLUtil import mxnet_model_io
    from local_misc import load_config
    config = load_config('hyperparameter_gan_common.json')(verbose_shape = True)
    input_shapes_gen = { 'latent' : (config.batch_size_gen, config.latent_size) }
    input_shapes_dis = { 'content' : (config.batch_size_dis, config.channels, config.image_height, config.image_width) }
    print('=' * 40 + '\nsymbol_gen:'); symbol_gen = get_symbol_gen(config(input_shapes = input_shapes_gen))
    print('=' * 40 + '\nsymbol_dis:'); symbol_dis = get_symbol_dis(config(input_shapes = input_shapes_dis))
    print('=' * 40)
    executor_gen = mxnet_model_io.get_shared_executor(config(
        symbol = symbol_gen,
        ctx = mx.gpu(config.gpuid),
        input_shapes = input_shapes_gen,
    ))
    executor_dis = mxnet_model_io.get_shared_executor(config(
        symbol = symbol_dis,
        ctx = mx.gpu(config.gpuid),
        input_shapes = input_shapes_dis,
    ))
