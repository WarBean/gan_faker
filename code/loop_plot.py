import os
import sys
import time
import glob

pattern = sys.argv[1]
file_set = set()
while True:
    curr_file_list = glob.glob(pattern)
    curr_file_set = set(curr_file_list)
    extra_file_list = sorted(curr_file_set - file_set)
    file_set.clear()
    file_set = curr_file_set
    for path in extra_file_list:
        print(path)
        os.system('imgcat %s' % path)
    time.sleep(1)
