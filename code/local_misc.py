from MLUtil.config import Config

def load_config(common_json, specific_json):
    common_config = Config(common_json) if common_json != None else Config()
    specific_config = Config(specific_json) if specific_json != None else Config()
    config = common_config.inplace_overwrite(**specific_config)
    config.batch_size_fake = config.batch_size_gen
    config.batch_size_real = config.batch_size_dis - config.batch_size_fake
    return config
