import symbol
import random
import mxnet as mx
import numpy as np
from PIL import Image
from queue import deque
from MLUtil import misc
from MLUtil import mxnet_misc
from MLUtil import mxnet_model_io

asnumpy = True

class GAN():

    def __init__(self, config, loader):
        self.config = config
        self.loader = loader
        self.create_executor(config)
        self.create_updater(config)
        self.create_statistic_window(config)
        self.latent_sample = mx.random.normal(loc = 0, scale = config.latent_stdvar, ctx = self.ctx, shape = (config.batch_size_vbn, 100))
        self.mbf_zero = mx.nd.zeros(self.input_shapes_mbf['real_mbf'], ctx = self.ctx)
        real_ones = mx.nd.ones(config.batch_size_real, ctx = self.ctx)
        fake_ones = mx.nd.ones(config.batch_size_fake, ctx = self.ctx)
        if config.improve_technique.label_smoothing:
            self.smooth_label_for_dis = mx.nd.concatenate([real_ones * 0.9, fake_ones * 0.0])
            self.smooth_label_for_gen = mx.nd.concatenate([real_ones * 0.0, fake_ones * 0.9])
        else:
            self.smooth_label_for_dis = mx.nd.concatenate([real_ones * 1.0, fake_ones * 0.0])
            self.smooth_label_for_gen = mx.nd.concatenate([real_ones * 0.0, fake_ones * 1.0])
        self.history_arg_dict = dict()

    def create_executor(self, config):
        if config.gpuid == -1: self.ctx = mx.cpu(0)
        else: self.ctx = mx.gpu(config.gpuid)
        normal_init = mx.init.Normal(sigma = config.weight_scale)
        initializer_list = [
            [mxnet_misc.name_hasnot_keyword(['latent', 'content', 'label']), normal_init],
        ]
        input_shapes_vbn = { 'latent'  : (config.batch_size_vbn, 100) }
        input_shapes_gen = { 'latent'  : (config.batch_size_gen, 100) }
        input_shapes_dis = { 'content' : (config.batch_size_dis, config.channels, config.image_height, config.image_width) }
        input_shapes_clf = { 'data' : (config.batch_size_dis, 1) }
        if config.improve_technique.virtual_batch_normalization:
            momentum_vbn, momentum_gen = 1.0, 0.0
        else:
            momentum_vbn, momentum_gen = 0.0, 0.9
        self.symbol_vbn = symbol.get_symbol_vbn(config(input_shapes = input_shapes_vbn, momentum = momentum_vbn))
        self.symbol_gen = symbol.get_symbol_gen(config(input_shapes = input_shapes_gen, momentum = momentum_gen))
        self.symbol_dis = symbol.get_symbol_dis(config(input_shapes = input_shapes_dis, momentum = 0.9))
        self.symbol_clf = symbol.get_symbol_clf(config(input_shapes = input_shapes_clf))
        self.input_shapes_mbf = { 'real_mbf': self.symbol_dis.infer_shape(**input_shapes_dis)[1][1] }
        self.symbol_mbf = symbol.get_symbol_mbf(config(input_shapes = self.input_shapes_mbf))
        self.executor_vbn = mxnet_model_io.get_shared_executor(config(
            symbol = self.symbol_vbn,
            ctx = self.ctx,
            input_shapes = input_shapes_vbn,
            initializer_list = initializer_list,
        ))
        self.executor_gen = mxnet_model_io.get_shared_executor(config(
            symbol = self.symbol_gen,
            ctx = self.ctx,
            input_shapes = input_shapes_gen,
            shared_stuff = self.executor_vbn,
            share_name_pred = (lambda name: name != 'latent'),
        ))
        self.executor_dis = mxnet_model_io.get_shared_executor(config(
            symbol = self.symbol_dis,
            ctx = self.ctx,
            input_shapes = input_shapes_dis,
            initializer_list = initializer_list,
        ))
        self.executor_clf = mxnet_model_io.get_shared_executor(config(
            symbol = self.symbol_clf,
            ctx = self.ctx,
            input_shapes = input_shapes_clf,
        ))
        self.executor_mbf = mxnet_model_io.get_shared_executor(config(
            symbol = self.symbol_mbf,
            ctx = self.ctx,
            input_shapes = self.input_shapes_mbf,
        ))

    def create_updater(self, config):
        lr_sum = config.learning_rate
        lr_dis = lr_sum * config.learning_rate_factor_dis
        lr_gen = lr_sum - lr_dis
        self.opt_dis = mx.optimizer.Adam(
            learning_rate = lr_dis,
            wd = config.weight_decay,
            rescale_grad = 1.0 / config.batch_size_dis)
        self.updater_dis = mx.optimizer.get_updater(self.opt_dis)
        self.opt_gen = mx.optimizer.Adam(
            learning_rate = lr_gen,
            wd = config.weight_decay,
            rescale_grad = 1.0 / config.batch_size_gen)
        self.updater_gen = mx.optimizer.get_updater(self.opt_gen)

    def create_statistic_window(self, config):
        self.real_loss_window = deque(maxlen = config.window_length)
        self.fake_loss_window = deque(maxlen = config.window_length)
        self.real_accu_window = deque(maxlen = config.window_length)
        self.fake_accu_window = deque(maxlen = config.window_length)
        self.mbf_error_window = deque(maxlen = config.window_length)
        self.who_window       = deque(maxlen = 100)

    def provide_content_for_dis(self, fake_content):
        batch = self.loader.next_batch()
        real_content = mx.nd.array(batch.data, ctx = self.ctx)
        content = mx.nd.concatenate([real_content, fake_content])
        return content

    def historical_averaging_grad(self, name, arg):
        if name not in self.history_arg_dict:
            grad = mx.nd.zeros(arg.shape, ctx = self.ctx)
            self.history_arg_dict[name] = arg.copy()
        else:
            grad = arg - self.history_arg_dict[name]
            p = self.config.ha_decay_factor
            self.history_arg_dict[name] = p * self.history_arg_dict[name] + (1 - p) * arg
        return grad

    def train_dis(self):
        # forward
        mx.random.normal(loc = 0, scale = self.config.latent_stdvar, out = self.executor_gen.arg_dict['latent'])
        self.executor_gen.forward(is_train = False)
        fake_content = self.executor_gen.outputs[0]
        content = self.provide_content_for_dis(fake_content)
        self.executor_dis.arg_dict['content'][:] = content
        self.executor_dis.forward(is_train = True)
        self.executor_clf.arg_dict['data'][:] = self.executor_dis.outputs[0]
        self.executor_clf.arg_dict['label'][:] = self.smooth_label_for_dis
        self.executor_clf.forward(is_train = True)
        #if asnumpy: self.executor_clf.outputs[0].asnumpy()
        # backward
        self.executor_clf.backward()
        clf_grad = self.executor_clf.grad_dict['data']
        self.executor_dis.backward(out_grads = [clf_grad, self.mbf_zero, self.mbf_zero])
        for i, name in enumerate(self.symbol_dis.list_arguments()):
            if name not in ['content']:
                arg = self.executor_dis.arg_dict[name]
                grad = self.executor_dis.grad_dict[name]
                if self.config.improve_technique.historical_averaging:
                    grad += self.historical_averaging_grad(name, arg)
                if asnumpy: grad.asnumpy()
                self.updater_dis(i, grad, arg)

    def train_gen(self):
        # forward
        mx.random.normal(loc = 0, scale = self.config.latent_stdvar, out = self.executor_gen.arg_dict['latent'])
        self.executor_gen.forward(is_train = True)
        content = self.provide_content_for_dis(self.executor_gen.outputs[0])
        self.executor_dis.arg_dict['content'][:] = content
        self.executor_dis.forward(is_train = True)
        self.executor_clf.arg_dict['data'][:] = self.executor_dis.outputs[0]
        self.executor_clf.arg_dict['label'][:] = self.smooth_label_for_gen
        self.executor_clf.forward(is_train = True)
        if self.config.improve_technique.feature_matching:
            self.executor_mbf.arg_dict['real_mbf'][:] = self.executor_dis.outputs[1]
            self.executor_mbf.arg_dict['fake_mbf'][:] = self.executor_dis.outputs[2]
            self.executor_mbf.forward(is_train = True)
        #if asnumpy: self.executor_gen.outputs[0].asnumpy()
        #if asnumpy: self.executor_clf.outputs[0].asnumpy()
        # backward
        self.executor_clf.backward()
        clf_grad = self.executor_clf.grad_dict['data']
        self.executor_mbf.backward()
        if self.config.improve_technique.feature_matching:
            real_mbf_grad = self.executor_mbf.grad_dict['real_mbf']
            fake_mbf_grad = self.executor_mbf.grad_dict['fake_mbf']
        else:
            real_mbf_grad = self.mbf_zero
            fake_mbf_grad = self.mbf_zero
        self.executor_dis.backward(out_grads = [clf_grad, real_mbf_grad, fake_mbf_grad])
        content_grad = self.executor_dis.grad_dict['content'][self.config.batch_size_real : self.config.batch_size_real + self.config.batch_size_gen]
        self.executor_gen.backward(out_grads = content_grad)
        for i, name in enumerate(self.symbol_gen.list_arguments()):
            if name not in ['latent']:
                arg = self.executor_gen.arg_dict[name]
                grad = self.executor_gen.grad_dict[name]
                if self.config.improve_technique.historical_averaging:
                    grad += self.historical_averaging_grad(name, arg)
                if asnumpy: grad.asnumpy()
                self.updater_gen(i, grad, arg)

    def _train(self):
        self.train_dis()
        self.train_gen()
        fake_accu = misc.mean(self.fake_accu_window, default = 0.5)
        if fake_accu < 0.5:
            self.who_window.append(0)
            return 'G'
        else:
            self.who_window.append(1)
            return 'D'

    def train(self):
        fake_accu = misc.mean(self.fake_accu_window, default = 0.5)
        if fake_accu < self.config.switch_fake_accu:
            self.train_dis()
            who = 'D'
        elif fake_accu > self.config.switch_fake_accu:
            self.train_gen()
            who = 'G'
        elif random.random() < 0.5:
            self.train_dis()
            who = 'D'
        else:
            self.train_gen()
            who = 'G'
        self.who_window.append(who == 'D')
        return who

    def update_vbn(self):
        self.executor_vbn.arg_dict['latent'][:] = self.latent_sample
        self.executor_vbn.forward(is_train = True)
        self.executor_vbn.backward()

    def evaluate(self):
        mx.random.normal(loc = 0, scale = self.config.latent_stdvar, out = self.executor_gen.arg_dict['latent'])
        self.executor_gen.forward(is_train = False)
        content = self.provide_content_for_dis(self.executor_gen.outputs[0])
        self.executor_dis.arg_dict['content'][:] = content
        self.executor_dis.forward(is_train = False)
        self.executor_clf.arg_dict['data'][:] = self.executor_dis.outputs[0]
        self.executor_clf.forward(is_train = False)
        prob_output = self.executor_clf.outputs[0].asnumpy()
        real_prob_output = prob_output[: self.config.batch_size_real]
        fake_prob_output = prob_output[self.config.batch_size_real :]
        #real_loss = -np.mean(np.log(real_prob_output))
        #fake_loss = -np.mean(np.log(1 - fake_prob_output))
        real_accu = np.mean(real_prob_output > 0.5)
        fake_accu = np.mean(fake_prob_output < 0.5)
        self.real_accu_window.append(real_accu)
        self.fake_accu_window.append(fake_accu)
        real_mbf_output = self.executor_dis.outputs[1].asnumpy()
        fake_mbf_output = self.executor_dis.outputs[2].asnumpy()
        mbf_error = np.mean((real_mbf_output - fake_mbf_output) ** 2)
        self.mbf_error_window.append(mbf_error)

    def save_checkpoint(self, save_id):
        checkpoint_path = '../model/%s-%04d.params' % (self.config.checkpoint_prefix, save_id)
        mxnet_model_io.save_checkpoint(checkpoint_path, [self.executor_gen, self.executor_dis], mxnet_misc.name_hasnot_keyword(['latent', 'content', 'label']))
        return checkpoint_path

    def visualize_fake(self, it):
        self.executor_vbn.arg_dict['latent'][:] = self.latent_sample
        self.executor_vbn.forward(is_train = False)
        output = self.executor_vbn.outputs[0].asnumpy()
        if output.shape[1] == 1: output = output[:, 0]
        else: output = output.swapaxes(1, 2).swapaxes(2, 3)
        output = misc.safely_to_uint8(output * 128 + 128)
        if self.config.visualize_style == 'separate':
            for sample_index, in range(self.config.batch_size_vis):
                data = output[sample_index]
                image = Image.fromarray(data)
                image.save('%sfake-iter%06d-sample%04d.jpg' % (self.config.visualize_prefix, it, sample_index))
        elif self.config.visualize_style == 'concat':
            data = np.hstack([output[sample_index] for sample_index in range(self.config.batch_size_vis)])
            image = Image.fromarray(data)
            image.save('%sfake-iter%06d.jpg' % (self.config.visualize_prefix, it))
        else:
            assert False

    def decay_learning_rate(self):
        self.opt_dis.lr /= self.config.lr_decay_factor
        self.opt_gen.lr /= self.config.lr_decay_factor

    def fake_loss(self): return misc.mean(self.fake_loss_window, default = -1)
    def real_loss(self): return misc.mean(self.real_loss_window, default = -1)
    def fake_accu(self): return misc.mean(self.fake_accu_window, default = -1)
    def real_accu(self): return misc.mean(self.real_accu_window, default = -1)
    def mbf_error(self): return misc.mean(self.mbf_error_window, default = -1)
    def D_ratio(self):   return misc.mean(self.who_window)
    def G_ratio(self):   return 1 - self.D_ratio()

if __name__ == '__main__':
    from local_misc import load_config
    config = load_config('hyperparameter_gan_common.json', None)
    gan = GAN(config, None)
